/*
	Когда были построены мои дома на Бейкер стрит и Малхолланд Драйв?
*/

select b.location as 'Расположение',
	b.end_date_building as 'Дата окончания стройки'
from buildings b
where b.location in ('Бейкер стрит', 'Малхолланд Драйв');