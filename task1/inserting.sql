/*
	Добавляет несколько записей с каждую таблицу (для тестов)
*/
use attask;

-- Информация о строениях
insert into buildings 
(location, rent_cost, sell_cost, target, end_date_building)
values
('Бейкер стрит', 15, 1500, 'selling', '2008-7-04');

insert into buildings 
(location, rent_cost, sell_cost, target, end_date_building)
values
('Малхолланд Драйв', 21, 2700, 'rent', '2012-4-13');

insert into buildings 
(location, rent_cost, sell_cost, target, end_date_building)
values
('Бейкер стрит', 33, 6200, 'rent', '2015-2-04');

insert into buildings 
(location, rent_cost, sell_cost, target, end_date_building)
values
('Союза-республик', 12, 1200, 'selling', '2017-1-21');


-- Информация о клиентах
insert into clients 
(fio, phone)
values
('Андрей', '89139863315');

insert into clients 
(fio)
values
('Юнона');

insert into clients 
(fio, phone)
values
('Борис', '71633');


-- Информация о сделках
insert into deals 
(deal_type, cost, transaction_date, client_id, building_id)
values
('selling', 1500, '2008-12-13', 1, 1);

insert into deals 
(deal_type, cost, transaction_date, client_id, building_id)
values
('rent', 20, '2012-6-23', 2, 2);

insert into deals 
(deal_type, cost, transaction_date, client_id, building_id)
values
('rent', 33, '2015-11-11', 3, 3);


-- Информация об арендах
insert into rent_information 
(pay_date, last_pay_date, deal_id)
values
(23, '2016-11-22', 2);

insert into rent_information 
(pay_date, last_pay_date, deal_id)
values
(11, '2017-2-13', 3);
