/*
	Сколько у меня арендаторов и кто, когда и сколько платит?
*/

set @counter=0; -- Счетчик арендаторов

select @counter:=@counter + 1 as '№',
	c.fio as 'Клиент', 
	r.pay_date as 'день оплаты', 
    d.cost as 'стоймость (уе)'
from clients c, deals d, rent_information r
where c.id = d.client_id
and d.id = r.deal_id
