/*
	Сколько стоит всё моё недвижимое имущество?
*/

-- строймость всех, даже уже проданных, построек
set @all = (select sum(b.sell_cost)
	from buildings b);

-- стоймость построек, которые еще принадлежат строителю (еще не проданы)
set @not_sold = (select sum(b.sell_cost) 
	from buildings b
	where b.id not in (select d.building_id 
							from deals d 
							where d.deal_type = 'selling'));
                        
select @all  as 'Общая стоймость всех построек (уе)',
	@not_sold as 'Общая стоймость не проданных построек (уе)';