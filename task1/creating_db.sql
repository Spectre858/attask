/*
	Создает базу данных (attask) с и 4 таблицы
    (строения, клиенты, сделки и информация об аренде)
*/


drop database if exists attask;
create database attask;
use attask;

-- Строения
create table buildings (
	id int not null auto_increment primary key,
    location varchar(80) not null, 		-- Расположение здания (улица)
    rent_cost double unsigned not null, -- Цена аренды
    sell_cost double unsigned not null, -- Цена продажи
    target varchar(20) not null,		-- Назначение, зачем строится (на продажу или в аренду)
    end_date_building date not null		-- Дата окончания строительства
) ENGINE=InnoDB;

-- Клиенты 
create table clients (
	id int not null auto_increment primary key,
    fio varchar(80),	-- ФИО клиента
    phone varchar(11)	-- Телефон для связи 
) ENGINE=InnoDB;

-- Сделки
create table deals (
	id int not null auto_increment primary key,
    deal_type varchar(20) not null,			-- Тип сделки (продажа / аренда)
    cost double unsigned not null,			-- Цена вопроса
	transaction_date date not null,			-- Дата совершения сделки
    client_id int not null,					-- Ссылочка на клиента
    building_id int not null,				-- ссылочка на постройку
    foreign key fk_client(client_id) references clients(id) on delete restrict,
    foreign key fk_building(building_id) references buildings(id) on delete restrict
) ENGINE=InnoDB;


-- Информация об арендах
create table rent_information (
	id int not null auto_increment primary key,
    pay_date int unsigned not null,		-- Оговоренный день оплаты
    last_pay_date date,			-- Дата последней оплаты
    deal_id int not null,		-- Ссылочка на сделку
    foreign key fk_deal(deal_id) references deals(id) on delete restrict
) ENGINE=InnoDB;