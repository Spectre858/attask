/*
	Сколько у меня домов в аренде, а сколько на продажу и сколько я уже продал?
*/

-- Здания в аренде
set @in_rent=(select count(b.id) 
	from buildings b, deals d
    where b.id = d.building_id
    and d.deal_type = 'rent');
    
-- Проданные здания
set @saled=(select count(b.id) 
	from buildings b, deals d
    where b.id = d.building_id
    and d.deal_type = 'selling');
    
/* 
	Здания которые не учавствуют в сделках (не проданные и не в аренде)
	и которые построены на продажу
*/
set @to_sale=(select count(b.id) 
	from buildings b
    where b.target = 'selling'
    and b.id not in (select b.id 
						from buildings b, deals d
						where b.id = d.building_id));
    
select @in_rent as 'В аренде', 
	@saled as 'Продано',
    @to_sale as 'На продажу';
    