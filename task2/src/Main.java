import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Вычисляемые значения вводятся с клавиатуры
 * Вычисляется построчно
 */
public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        Parser parser = new Parser(input);
        parser.parse();
    }
}
