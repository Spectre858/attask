/**
 * Лексический анализатор
 */
public class Lexer {
    /**
     * Возможные состояния анализатора
     */
    private enum States {
        BEGIN,
        INT_DIGIT,
        REAL_DIGIT,
        OPERATION,
        ERROR
    }

    /**
     * Текущее состояние анализатора
     */
    private States currentState;
    /**
     * Анализируемая строка
     */
    private String line;
    /**
     * Смещение в строке
     */
    private int offset;

    /**
     * Значение токена
     */
    private StringBuilder tokenText;

    /**
     * @param line Анализируемая строка
     */
    public Lexer(String line) {
        this.line = line;
        this.currentState = States.BEGIN;
        this.offset = 0;
        this.tokenText = new StringBuilder();
    }

    /**
     * @return следующий распознаный токен
     * @see Symbol
     */
    public Symbol nextToken() {
        Symbol result = null;
        switch (currentState) {
            case BEGIN:
                result = parseBegin();
                break;
            case INT_DIGIT:
                result = parseIntDigit();
                break;
            case REAL_DIGIT:
                result = parseRealDigit();
                break;
            case OPERATION:
                result = parseOperation();
                break;
            case ERROR:
                result = parseError();
                break;
        }

        return result;
    }

    /**
     * Парсер для стартового состояние автомата
     */
    private Symbol parseBegin() {
        tokenText = new StringBuilder();
        char ch;

        while (offset < line.length()) {
            ch = line.charAt(offset++);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    tokenText.append(ch);
                    currentState = States.INT_DIGIT;
                    return parseIntDigit();
                case '+':
                case '-':
                case '*':
                case '/':
                    tokenText.append(ch);
                    currentState = States.OPERATION;
                    return parseOperation();
                case ' ':
                case '\t':
                case '\f':
                case '\n':
                case '\r':
                    break;
                default:
                    tokenText.append(ch);
                    currentState = States.ERROR;
                    return parseError();
            }
        }
        return new Symbol(Symbol.Tokens.END_OF_LINE);
    }

    /**
     * Парсер для состояния, соответствуюего целому числу
     */
    private Symbol parseIntDigit() {
        char ch;

        while (offset < line.length()) {
            ch = line.charAt(offset++);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    tokenText.append(ch);
                    break;
                case '.':
                    tokenText.append(ch);
                    currentState = States.REAL_DIGIT;
                    return parseRealDigit();
                default:
                    offset--;
                    currentState = States.BEGIN;
                    return new Symbol(Symbol.Tokens.DIGIT, tokenText.toString());
            }
        }
        currentState = States.BEGIN;
        return new Symbol(Symbol.Tokens.DIGIT, tokenText.toString());
    }

    /**
     * Парсер для состояния, соответствуюего вещественному числу
     */
    private Symbol parseRealDigit() {
        boolean mustDigit = true;
        char ch;

        while (offset < line.length()) {
            ch = line.charAt(offset++);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    tokenText.append(ch);
                    mustDigit = false;
                    break;
                case '.':
                    tokenText.append(ch);
                    currentState = States.ERROR;
                    return parseError();
                default:
                    if (mustDigit) {
                        tokenText.append(ch);
                        currentState = States.ERROR;
                        return parseError();
                    }
                    offset--;
                    currentState = States.BEGIN;
                    return new Symbol(Symbol.Tokens.DIGIT, tokenText.toString());
            }
        }
        if (mustDigit) {
            currentState = States.ERROR;
            return parseError();
        }
        currentState = States.BEGIN;
        return new Symbol(Symbol.Tokens.DIGIT, tokenText.toString());
    }

    /**
     * Парсер для операций
     */
    private Symbol parseOperation() {
        Symbol.Tokens resultToken = null;
        String text = tokenText.toString();
        switch (tokenText.toString()) {
            case "+":
                resultToken = Symbol.Tokens.PLUS;
                break;
            case "-":
                resultToken = Symbol.Tokens.MINUS;
                break;
            case "*":
                resultToken = Symbol.Tokens.MUL;
                break;
            case "/":
                resultToken = Symbol.Tokens.DIV;
                break;
        }

        if (offset >= line.length()) {
            currentState = States.ERROR;
            return parseError();
        }
        char ch = line.charAt(offset);
        switch (ch) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case ' ':
            case '\t':
            case '\f':
            case '\n':
            case '\r':
                break;
            default:
                tokenText.append(ch);
                currentState = States.ERROR;
                return parseError();
        }

        currentState = States.BEGIN;
        return new Symbol(resultToken, text);
    }

    /**
     * Возвращает ошибку
     */
    private Symbol parseError() {
        return new Symbol(Symbol.Tokens.ERROR, tokenText.toString());
    }

}
