/**
 * Модель символа
 */
public class Symbol {
    /**
     * Все возможные токены
     */
    public enum Tokens {
        PLUS,
        MINUS,
        MUL,
        DIV,
        DIGIT,
        END_OF_LINE,
        ERROR
    }

    public Tokens token;
    public String text;

    public Symbol(Tokens token, String text) {
        this.token = token;
        this.text = text;
    }

    public Symbol(Tokens token) {
        this.token = token;
        text = "";
    }

    @Override
    public String toString() {
        return String.format("[%s] - %s", token, text);
    }
}
