import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Синтаксический анализатор
 */
public class Parser {
    /**
     * Возможные состояния анализатора
     */
    private enum States {
        BEGIN,
        DIGIT,
        OPERATION,
        END_OF_LINE,
        ERROR
    }
    private BufferedReader input;

    /**
     * Результирующие буферы с числами и операциями
     */
    private LinkedList<Double> digitList;
    private LinkedList<Symbol.Tokens> operationList;

    /**
     * Текущее состояние анализатора
     */
    private States currentState;

    /**
     * @param input Reader из которой построчно будут доставаться
     *              последовательности и анализироваться
     */
    public Parser(BufferedReader input) {
        this.input = input;
        this.digitList = new LinkedList<>();
        this.operationList = new LinkedList<>();
        this.currentState = States.BEGIN;
    }

    /**
     * Основная функция, осуществляет синтаксический анализ
     * по токенам, полученым от лексического анализатора
     * @see Lexer
     * После каждой проверенной строчки вычисляет результат
     */
    public void parse() throws IOException {
        String line;
        Lexer lexer;

        while ((line = input.readLine()) != null) {
            currentState = States.BEGIN;
            lexer = new Lexer(line);
            Symbol symbol;

            while (!States.ERROR.equals(currentState)
                    && !States.END_OF_LINE.equals(currentState)) {

                symbol = lexer.nextToken();

                switch (symbol.token) {
                    case DIGIT:
                        if (States.DIGIT.equals(currentState)) {
                            printError(symbol);
                            break;
                        }
                        digitList.addLast(new Double(symbol.text));
                        currentState = States.DIGIT;
                        break;
                    case PLUS:
                    case MINUS:
                    case MUL:
                    case DIV:
                        if (States.OPERATION.equals(currentState)) {
                            printError(symbol);
                            break;
                        }
                        operationList.addLast(symbol.token);
                        currentState = States.OPERATION;
                        break;
                    case END_OF_LINE:
                        if (!States.DIGIT.equals(currentState)
                                && !States.BEGIN.equals(currentState)) {
                            printError(symbol);
                            break;
                        }
                        currentState = States.END_OF_LINE;
                        break;
                    case ERROR:
                        printError(symbol);
                        break;

                }
            }
            if (!States.ERROR.equals(currentState)) {
                calculate();
            }
            digitList = new LinkedList<>();
            operationList = new LinkedList<>();
        }
    }

    /**
     * Выводит содержимое результирующих буферов с числами и операциями
     */
    public void printBuffers() {
        System.out.println(digitList);
        System.out.println(operationList);
    }

    /**
     * Выводит сообщение об ошибке
     * @param symbol символ из-за которого автомат упал в ошибку.
     */
    private void printError(Symbol symbol) {
        currentState = States.ERROR;
        System.err.println("Unexpected symbol: " + symbol);
    }

    /**
     * Вычисляет и выводит результат по результирующим буферам
     */
    private void calculate() {
        if (digitList.isEmpty() || operationList.isEmpty()
                || digitList.size() <= operationList.size()) {
            return;
        }
        for (int i = 0; i < operationList.size(); ++i) {
            Symbol.Tokens operation = operationList.get(i);

            if (Symbol.Tokens.MUL.equals(operation)
                    || Symbol.Tokens.DIV.equals(operation)) {
                operationList.remove(i);
                Double rightDigit = digitList.remove(i + 1);
                Double leftDigit = digitList.remove(i);
                Double result = (Symbol.Tokens.MUL.equals(operation)
                        ? new Double(leftDigit.doubleValue() * rightDigit.doubleValue())
                        : new Double(leftDigit.doubleValue() / rightDigit.doubleValue()));

                digitList.add(i, result);
                --i;
            }
        }
        double result = digitList.get(0);

        for (int i = 0; i < operationList.size(); ++i) {
            Symbol.Tokens operation = operationList.get(i);
            Double rightDigit = digitList.get(i + 1);

            result = (Symbol.Tokens.PLUS.equals(operation)
                    ? result + rightDigit.doubleValue()
                    : result - rightDigit.doubleValue());
        }
        System.out.println("Result: " + result);
    }
}
